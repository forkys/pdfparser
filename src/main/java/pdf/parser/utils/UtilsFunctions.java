package pdf.parser.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.reflections.Reflections;
import pdf.parser.ml.classifiers.Classifiers;
import pdf.parser.properties.MyProperties;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.stream.Collectors;

public class UtilsFunctions {

    // static variable single_instance of type Singleton
    private static UtilsFunctions single_instance = null;

    // static method to create instance of Singleton class
    public static UtilsFunctions getInstance()
    {

        if (single_instance == null)
            single_instance = new UtilsFunctions();

        return single_instance;
    }

    Reflections reflections = new Reflections(Classifiers.class);
    public <T> Set<String> getAllClassifiers(Class<T> clazz) {
        Set<Class<? extends T>> classes = this.reflections.getSubTypesOf(clazz);

        if (classes.size() <= 0) return new HashSet<>();

        Set<String> extending = classes.stream().map(aClass -> {
            try {
                return aClass.getName();
            } catch (Exception e) {
                return null;
            }
        }).collect(Collectors.toSet());
        return extending;
    }

    public JsonArray extractParts(Elements elements) {

        JsonArray results = new JsonArray();

        for(Element element: elements){
            if(!element.attr("style").equals("")){
                JsonObject textFileArff = new JsonObject();
                String[] valuesStyle = element.attr("style").split(";");
                for(String value: valuesStyle){
                    String key = value.split(":")[0];
                    if(key.equals("font-size") || key.equals("font-weight") || key.equals("font-style")){
                        textFileArff.addProperty(key,(value.split(":")[1].replace("%","").replace(" ","")).toString());
                    }
                }
                textFileArff.addProperty("text", element.text());
                results.add(textFileArff);
            }

        }
        return results;
    };

    public void addNewRowsDataSet(ArrayList<LinkedHashMap<String,String>> newRows ,String label) throws IOException {
        String csv = MyProperties.getInstance().getProperty("path.dataset.csv");
        FileWriter writer = new FileWriter(csv, true);

        int left;
        String leftValue;
        int top;
        String topValue;
        int fontSize;
        String fontSizeValue;
        int fontWeight;
        String fontWeightValue;
        int fontStyle;
        String fontStyleValue;
        int fontFamily;
        String fontFamilyValue;
        int transform;
        String transformValue;

        for(LinkedHashMap<String,String> newRow: newRows) {

            if (newRow.get("top") == null) {
                top = 0;
                topValue = "?";
            } else {
                top = 1;
                topValue = newRow.get("top").toString();
            }

            if (newRow.get("left") == null) {
                left = 0;
                leftValue = "?";
            } else {
                left = 1;
                leftValue = newRow.get("left").toString();
            }

            if (newRow.get("font-family") == null) {
                fontFamily = 0;
                fontFamilyValue = "?";
            } else {
                fontFamily = 1;
                fontFamilyValue = newRow.get("font-family").toString();
            }

            if (newRow.get("transform") == null) {
                transform = 0;
                transformValue = "?";
            } else {
                transform = 1;
                transformValue = newRow.get("transform").toString();
            }

            if (newRow.get("font-size") == null) {
                fontSize = 0;
                fontSizeValue = "?";
            } else {
                fontSize = 1;
                fontSizeValue = newRow.get("font-size").toString();
            }

            if (newRow.get("font-weight") == null) {
                fontWeight = 0;
                fontWeightValue = "?";
            } else {
                fontWeight = 1;
                fontWeightValue = newRow.get("font-weight").toString();
            }

            if (newRow.get("font-style") == null) {
                fontStyle = 0;
                fontStyleValue = "?";
            } else {
                fontStyle = 1;
                fontStyleValue = newRow.get("font-style").toString();
            }

            String record = "\n" + transform + ";" + transformValue.replaceAll("\"", "") + ";" + fontFamily + ";" + fontFamilyValue.replaceAll("\"", "") + ";" + left + ";" + leftValue.replaceAll("\"", "") + ";" + top + ";" + topValue.replaceAll("\"", "") + ";" + fontSize + ";" + fontSizeValue.replaceAll("\"", "") + ";" + fontWeight + ";" + fontWeightValue.replaceAll("\"", "") + ";" + fontStyle + ";" + fontStyleValue.replaceAll("\"", "") + ";" + label;
            writer.write(record);
        }

        writer.close();
    }
}
