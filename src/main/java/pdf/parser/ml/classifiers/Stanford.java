package pdf.parser.ml.classifiers;

import com.google.gson.JsonObject;
import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.ling.Datum;
import edu.stanford.nlp.objectbank.ObjectBank;
import org.apache.commons.io.FileUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pdf.parser.properties.MyProperties;

import java.io.*;
import java.util.ArrayList;

public class Stanford implements Classifiers {

    ColumnDataClassifier cdc;

    public void createDatasetFromCsvToTest() throws IOException{
        FileReader fileReader = new FileReader(MyProperties.getInstance().getProperty("path.dataset.csv"));
        String inString;

        BufferedWriter bw = new BufferedWriter(new FileWriter(pathDataSet, false));
        BufferedWriter bwTest = new BufferedWriter(new FileWriter(pathDataSetTest, false));

        //int lineTraining = (int) Math.round(counter.lines().count()*0.8);
        BufferedReader inStream = new BufferedReader(fileReader);
        ArrayList<String> ans= new ArrayList<String>();

        // Read rows
        while ((inString = inStream.readLine()) != null) {
            ans.add(inString);
        }

        int lineTraining = (int) Math.round(ans.size()*0.8);
        int rowElaborated = 0;
        while (rowElaborated < lineTraining) {
         //while((inString = inStream.readLine()) != null){
            inString = ans.get(rowElaborated);
            String newLine = inString.split(";")[inString.split(";").length -1];
            for(int i=0; i < inString.split(";").length -1; i++){
                newLine = newLine.concat(";").concat(inString.split(";")[i]);
            }
            bw.append(newLine.replaceAll(";", MyProperties.getInstance().getProperty("path.stanford.separator")) + '\n');
            rowElaborated++;
        }
        bw.flush();

        while (rowElaborated < ans.size()) {
            inString = ans.get(rowElaborated);
            String newLine = inString.split(";")[inString.split(";").length -1];
            for(int i=0; i < inString.split(";").length -1; i++){
                newLine = newLine.concat(";").concat(inString.split(";")[i]);
            }
            bwTest.append(newLine.replaceAll(";", MyProperties.getInstance().getProperty("path.stanford.separator")) + '\n');
            rowElaborated++;
        }
        bwTest.flush();
        inStream.close();
    };

    public void createDatasetFromCsv() throws FileNotFoundException, IOException{
        FileReader fileReader = new FileReader(MyProperties.getInstance().getProperty("path.dataset.csv"));
        BufferedReader inStream = new BufferedReader(fileReader);
        String inString;

        BufferedWriter bw = new BufferedWriter(new FileWriter(pathDataSet, false));


        while ((inString = inStream.readLine()) != null) {
            String newLine = inString.split(";")[inString.split(";").length -1];
            for(int i=0; i < inString.split(";").length -1; i++){
                newLine = newLine.concat(";").concat(inString.split(";")[i]);
            }
            bw.append(newLine.replaceAll(";", MyProperties.getInstance().getProperty("path.stanford.separator")) + '\n');
        }
        bw.flush();
        inStream.close();
    };

    public JsonObject getPredictions(Elements elements) throws Exception {
        System.out.println("Testing predictions of ColumnDataClassifier");
        String nameFilePredicted = ""+ System.currentTimeMillis();
        String pathFilePredicted = MyProperties.getInstance().getProperty("path.stanford.tmp.predicted") + nameFilePredicted + ".test";
        File dataSet = new File(pathFilePredicted);
        BufferedWriter bw = new BufferedWriter(new FileWriter(dataSet, true));

        for(Element element: elements){
            if(!element.attr("style").equals("")){
                String[] valuesStyle = element.attr("style").split(";");

                int fontSize = 0;
                String fontSizeValue = "?";
                int fontWeight = 0;
                String fontWeightValue = "?";
                int fontStyle = 0;
                String fontStyleValue = "?";
                int left= 0;
                String leftValue = "?";
                int top= 0;
                String topValue = "?";
                int fontFamily= 0;
                String fontFamilyValue = "?";
                int transform= 0;
                String transformValue = "?";

                for(String value: valuesStyle){
                    switch(value.split(":")[0]){
                        case "left":{
                            left = 1;
                            leftValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "top":{
                            top = 1;
                            topValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "font-family":{
                            fontFamily = 1;
                            fontFamilyValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "transform":{
                            transform = 1;
                            transformValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "font-size":{
                            fontSize = 1;
                            fontSizeValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "font-weight":{
                            fontWeight = 1;
                            fontWeightValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "font-style":{
                            fontStyle = 1;
                            fontStyleValue = value.split(":")[1].replace("%","");
                            break;
                        }
                    }
                }
                //String row = element.text() + "\t" + fontSize + "\t" + fontSizeValue + "\t" + fontWeight + "\t" + fontWeightValue + "\t" + fontStyle + "\t" + fontStyleValue + "\n";
                String row = transform+ "\t" + transformValue+ "\t" + fontFamily+ "\t" +fontFamilyValue+ "\t" +left+ "\t" +leftValue+ "\t" +top+ "\t" +topValue+ "\t" +fontSize + "\t" + fontSizeValue + "\t" + fontWeight + "\t" + fontWeightValue + "\t"+fontStyle + "\t"+fontStyleValue +  "\n";
                bw.append(row);
            }
        }
        bw.close();

        JsonObject results = new JsonObject();

        int i=0;
        for (String line : ObjectBank.getLineIterator(pathFilePredicted, "utf-8")) {
            Datum<String,String> d = cdc.makeDatumFromLine(line);
            results.addProperty(line.split("\t")[0],cdc.classOf(d));
            i++;
        }


        return results;
    }

    public void executeTesting() throws Exception {
        pathDataSetTest = MyProperties.getInstance().getProperty("path.dataset") + MyProperties.getInstance().getProperty("path.stanford.dataset.test");
        createDatasetFromCsvToTest();
        System.out.println("Training ColumnDataClassifier");
        cdc = new ColumnDataClassifier(MyProperties.getInstance().getProperty("path.stanford.properties"));
        cdc.trainClassifier(pathDataSet);
        System.out.println("Testing ColumnDataClassifier");
        cdc.testClassifier(pathDataSetTest);
    }


    public void loadDataset() throws Exception {
        createDatasetFromCsv();
        System.out.println("Training ColumnDataClassifier");
        cdc = new ColumnDataClassifier(MyProperties.getInstance().getProperty("path.stanford.properties"));
        cdc.trainClassifier(pathDataSet);
    }

    // static variable single_instance of type Singleton
    private static Stanford single_instance = null;
    private static String pathDataSet;
    private static String pathDataSetTest;

    // static method to create instance of Singleton class
    public static Stanford getInstance() throws Exception
    {
        if (single_instance == null) {
            pathDataSet = MyProperties.getInstance().getProperty("path.dataset") + MyProperties.getInstance().getProperty("path.stanford.dataset");
            single_instance = new Stanford();
            single_instance.loadDataset();
        }

        return single_instance;
    }

}