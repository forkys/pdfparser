package pdf.parser.ml.classifiers;

import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pdf.parser.properties.MyProperties;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayesMultinomialText;
import weka.core.Instances;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Weka  implements Classifiers {

        Classifier classifier = new NaiveBayesMultinomialText();

    private static String pathDataSet;
    // static variable single_instance of type Singleton
    private static Weka single_instance = null;

    public JsonObject getPredictions(Elements elements) throws Exception{
        String nameFilePredicted = ""+ System.currentTimeMillis();
        String pathFilePredicted = MyProperties.getInstance().getProperty("path.weka.tmp.predicted") + nameFilePredicted + ".arff";
        File dataSet = new File(pathFilePredicted);

        BufferedWriter bw = new BufferedWriter(new FileWriter(dataSet, true));

        bw.append("@relation parts-document\n");
        bw.append("@attribute font-size {0,1}\n");
        bw.append("@attribute font-size-value string\n");
        bw.append("@attribute font-weight {0,1}\n");
        bw.append("@attribute font-weight-value string\n");
        bw.append("@attribute font-style {0,1}\n");
        bw.append("@attribute font-style-value string\n");
        bw.append("@attribute class {?}\n");
        bw.append("\n");
        bw.append("@data\n");

        for(Element element: elements){
            if(!element.attr("style").equals("")){
                String[] valuesStyle = element.attr("style").split(";");

                int fontSize = 0;
                String fontSizeValue = "?";
                int fontWeight = 0;
                String fontWeightValue = "?";
                int fontStyle = 0;
                String fontStyleValue = "?";

                int left= 0;
                String leftValue = "?";
                int top= 0;
                String topValue = "?";
                int fontFamily= 0;
                String fontFamilyValue = "?";
                int transform= 0;
                String transformValue = "?";

                for(String value: valuesStyle){
                    switch(value.split(":")[0]){
                        case "left":{
                            left = 1;
                            leftValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "top":{
                            top = 1;
                            topValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "font-family":{
                            fontFamily = 1;
                            fontFamilyValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "transform":{
                            transform = 1;
                            transformValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "font-size":{
                            fontSize = 1;
                            fontSizeValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "font-weight":{
                            fontWeight = 1;
                            fontWeightValue = value.split(":")[1].replace("%","");
                            break;
                        }
                        case "font-style":{
                            fontStyle = 1;
                            fontStyleValue = value.split(":")[1].replace("%","");
                            break;
                        }
                    }
                }
                String row = transform+ "," + transformValue+ "," + fontFamily+ "," +fontFamilyValue+ "," +left+ "," +leftValue+ "," +top+ "," +topValue+ "," +fontSize + "," + fontSizeValue + "," + fontWeight + "," + fontWeightValue + ","+fontStyle + ","+fontStyleValue + ",?" + "\n";
                bw.append(row);
            }
        }
        bw.close();
        Instances unlabeled = new Instances(new BufferedReader(new FileReader(pathFilePredicted)));
        unlabeled.setClassIndex(unlabeled.numAttributes()-1);
        String[] classes = MyProperties.getInstance().getProperty("pdf.elements").split(",");
        JsonObject results = new JsonObject();
        for (int i = 0; i < unlabeled.numInstances(); i++) {
            double clsLabel = classifier.classifyInstance(unlabeled.instance(i));
            try{
                results.addProperty(elements.get(i).text(),classes[(int) clsLabel]);
            }catch(IndexOutOfBoundsException e){}
        }

        return results;
    }

   public void createDatasetFromCsv() throws FileNotFoundException, IOException{
        FileReader fileReader = new FileReader(MyProperties.getInstance().getProperty("path.dataset.csv"));
        BufferedReader inStream = new BufferedReader(fileReader);
        String inString;

        BufferedWriter bw = new BufferedWriter(new FileWriter(pathDataSet, false));

        bw.append("@relation parts-document\n");
       bw.append("@attribute transform {0,1}\n");
       bw.append("@attribute transform-value string\n");
       bw.append("@attribute font-family {0,1}\n");
       bw.append("@attribute font-family-value string\n");
       bw.append("@attribute left {0,1}\n");
       bw.append("@attribute left-value string\n");
       bw.append("@attribute top {0,1}\n");
       bw.append("@attribute top-value string\n");
       bw.append("@attribute font-size {0,1}\n");
        bw.append("@attribute font-size-value string\n");
        bw.append("@attribute font-weight {0,1}\n");
        bw.append("@attribute font-weight-value string\n");
        bw.append("@attribute font-style {0,1}\n");
        bw.append("@attribute font-style-value string\n");
        bw.append("@attribute class {" + MyProperties.getInstance().getProperty("pdf.elements") + "}\n");
        bw.append("\n");
        bw.append("@data\n");

        while ((inString = inStream.readLine()) != null) {
            bw.append(inString.replaceAll(";", MyProperties.getInstance().getProperty("path.weka.separator")) + '\n');
        }
        bw.flush();
        inStream.close();
    }

    public void executeTesting() throws Exception {
        createDatasetFromCsv();
        /** load training set **/
        Instances trainingSet = new Instances(new BufferedReader(new FileReader(pathDataSet)));
        int trainSize = (int) Math.round(trainingSet.numInstances() * 0.8);
        int testSize = trainingSet.numInstances() - trainSize;
        Instances train = new Instances(trainingSet, 0, trainSize);
        train.setClassIndex(train.numAttributes()-1);
        Instances test = new Instances(trainingSet, trainSize, testSize);
        test.setClassIndex(test.numAttributes()-1);

        classifier.buildClassifier(train);

        Evaluation eval = new Evaluation(train);
       // eval.evaluateModel(classifier, train);
        //System.out.println("1-NN accuracy on training data:\n" + eval.pctCorrect()/100);
        eval.evaluateModel(classifier, test);
        //System.out.println("1-NN accuracy on separate test data:\n" + eval.pctCorrect()/100);
        System.out.println("*****" + eval.toSummaryString());
        System.out.println("*****" + eval.toClassDetailsString());

    }

    public void loadDataset() throws  Exception{

        createDatasetFromCsv();
        /** load training set **/
        Instances train = new Instances(new BufferedReader(new FileReader(pathDataSet)));
        train.setClassIndex(train.numAttributes()-1);//in my case the class was the first attribute thus zero otherwise it's the number of attributes -1

        //Evaluation eval = new Evaluation(train);
        //perform 10 fold cross validation
        //eval.crossValidateModel(classifier, train, 10, new Random(1));
        //String output = eval.toSummaryString();
        //System.out.println(output);

        //String classDetails = eval.toClassDetailsString();
        //System.out.println(classDetails);

        classifier.buildClassifier(train);
    }


    // static method to create instance of Singleton class
    public static Weka getInstance() throws Exception
    {
        if (single_instance == null) {
            pathDataSet = MyProperties.getInstance().getProperty("path.dataset") + MyProperties.getInstance().getProperty("path.weka.dataset");
            single_instance = new Weka();
            single_instance.loadDataset();
        }

        return single_instance;
    }

}
