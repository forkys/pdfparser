package pdf.parser.ml.classifiers;

import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.EmbeddingLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.evaluation.classification.ROC;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import pdf.parser.properties.MyProperties;

public class Deeplearning4j implements Classifiers {

    int seed = 123;
    static double learningRate = 0.001;
    int batchSize = 128;
    int nEpochs = 30;
    static int numInputs = 14;
    static int numOutputs = 4;
    static int numHiddenNodes = 20;
    static DataSetIterator trainIter;
    static MultiLayerConfiguration conf;
    static MultiLayerNetwork model;

    static String[] valuesTransform = new String[]{"?","none","matrix","matrix3d","translate","translate3d","translateX","translateY","translateZ","scale","scale3d","scaleX","scaleY","scaleZ","rotate","rotate3d","rotateX","rotateY","rotateZ","skew","skewX","skewY","perspective","initial","inherit"};
    /*** possible values css transform property ***/
    static Map<String, Integer> transformMap = new HashMap<String, Integer>();

    static String[] valuesFontFamily = new String[]{"?","Serif","Times New Roman","Georgia","Serif","Sans-serif","Arial","Verdana","Monospace","Courier New","Lucida Console"};
    /*** possible values css font-family property ***/
    static Map<String, Integer> fontFamilyMap = new HashMap<String, Integer>();


    static String[] valuesFontStyle = new String[]{"?","normal","italic","oblique","initial","inherit"};
    /*** possible values css font-style property ***/
    static Map<String, Integer> fontStyleMap = new HashMap<String, Integer>();

    static String[] valuesFontWeight = new String[]{"?","normal","bold","bolder","lighter","number","initial","inherit"};
    /*** possible values css font-style property ***/
    static Map<String, Integer> fontWeightMap = new HashMap<String, Integer>();

    static String[] classes = MyProperties.getInstance().getProperty("pdf.elements").split(",");
    /*** possible values css font-style property ***/
    static Map<Integer, String> classesMap = new HashMap<Integer, String>();

    private static String pathDataSet = MyProperties.getInstance().getProperty("path.dataset") + MyProperties.getInstance().getProperty("path.deeplearning.dataset");
    // static variable single_instance of type Singleton
    private static Deeplearning4j single_instance = null;


    public void executeTesting() throws Exception{
        createDatasetFromCsv();
       // String nameFilePredicted = ""+ System.currentTimeMillis();
       // String pathFilePredicted = MyProperties.getInstance().getProperty("path.deeplearning.tmp.predicted") + nameFilePredicted + ".csv";

        RecordReader rr = new CSVRecordReader(0, ';');
        rr.initialize(new FileSplit(new File(pathDataSet)));

        DataSetIterator iter = new RecordReaderDataSetIterator.Builder(rr, 128)
                .classification(14, 4)
                .build();

        DataSet next = iter.next();
        next.shuffle();
        SplitTestAndTrain testAndTrain = next.splitTestAndTrain(0.8);
        testAndTrain.getTrain().normalizeZeroMeanZeroUnitVariance();
        DataSet train = testAndTrain.getTrain();

        DataSet test = testAndTrain.getTest();

        DataNormalization normalizer = new NormalizerStandardize();
        normalizer.fit(train);           //Collect the statistics (mean/stdev) from the training data. This does not modify the input data
        normalizer.transform(train);     //Apply normalization to the training data
        normalizer.transform(test);
        Evaluation eval = new Evaluation();

        INDArray output = model.output(test.getFeatures());
        eval.eval(test.getLabels(), output);
        System.out.println(eval.stats());
    }

    public JsonObject getPredictions(Elements elements) throws Exception{
        String nameFilePredicted = ""+ System.currentTimeMillis();
        String pathFilePredicted = MyProperties.getInstance().getProperty("path.deeplearning.tmp.predicted") + nameFilePredicted + ".csv";
        File dataSet = new File(pathFilePredicted);

        BufferedWriter bw = new BufferedWriter(new FileWriter(dataSet, false));

        //for(int i=0; i < 2; i++){
        for(Element element: elements){
            if(!element.attr("style").equals("")){
                String[] valuesStyle = element.attr("style").split(";");

                int fontSize = 0;
                String fontSizeValue = "0";
                int fontWeight = 0;
                String fontWeightValue = "0";
                int fontStyle = 0;
                String fontStyleValue = "0";

                int left= 0;
                String leftValue = "0";
                int top= 0;
                String topValue = "0";
                int fontFamily= 0;
                String fontFamilyValue = "0";
                int transform= 0;
                String transformValue = "0";

                for(String value: valuesStyle){
                    String attribute = value.split(":")[1].replace("%","").replace("px","");
                    switch(value.split(":")[0]){
                        case "left":{
                            left = 1;
                            leftValue = parseAttribute(attribute).toString();
                            break;
                        }
                        case "top":{
                            top = 1;
                            topValue = parseAttribute(attribute).toString();
                            break;
                        }
                        case "font-family":{
                            fontFamily = 1;
                            fontFamilyValue = parseAttribute(attribute).toString();
                            break;
                        }
                        case "transform":{
                            transform = 1;
                            transformValue = parseAttribute(attribute).toString();
                            break;
                        }
                        case "font-size":{
                            fontSize = 1;
                            fontSizeValue = parseAttribute(attribute).toString();
                            break;
                        }
                        case "font-weight":{
                            fontWeight = 1;
                           // fontWeightValue = value.split(":")[1].replace("%","").replace("px","");
                            fontWeightValue = parseAttribute(attribute).toString();
                            break;
                        }
                        case "font-style":{
                            fontStyle = 1;
                            //fontStyleValue = value.split(":")[1].replace("%","").replace("px","");
                            fontStyleValue = parseAttribute(attribute).toString();
                            break;
                        }
                    }
                }
                String row = transform+ "," + transformValue+ "," + fontFamily+ "," +fontFamilyValue+ "," +left+ "," +leftValue+ "," +top+ "," +topValue+ "," +fontSize + "," + fontSizeValue + "," + fontWeight + "," + fontWeightValue + ","+fontStyle + ","+fontStyleValue+ ",0"  + "\n";
                //String row = fontSize + "," + fontSizeValue + "," + fontWeight + "," + fontWeightValue + ","+fontStyle + ","+fontStyleValue + ",?" + "\n";
                bw.append(row);
            }
        }

        bw.close();

        RecordReader rr = new CSVRecordReader(0, ',');
        rr.initialize(new FileSplit(new File(pathFilePredicted)));

        DataSetIterator iter = new RecordReaderDataSetIterator.Builder(rr, 128)
                .classification(14, 4)
                .build();

        //ROC roc = new ROC();
        //Evaluation eval = model.evaluate(iter);

        //Evaluation eval = new Evaluation(4); //create an evaluation object with 10 possible classes
       // while(iter.hasNext()){
            //DataSet next = iter.next();
            //INDArray output = model.output(next.getFeaturesMaskArray()); //get the networks prediction
            //eval.eval(next.getLabels(), output); //check the prediction against the true class
            //Evaluation eval = model.evaluate(iter);
            DataSet t = iter.next();
            INDArray features = t.getFeatures();
            INDArray resultsPredict = model.output(features,false);
            JsonObject results = new JsonObject();
            for (int i = 0; i < resultsPredict.rows(); i++) {
                //Iris irs = flowers.get(i);
                // set the classification from the fitted results
                results.addProperty(elements.get(i).text(),classesMap.get(maxIndex(getFloatArrayFromSlice(resultsPredict.slice(i)))));
                //irs.setIrisClass(classifiers.get(maxIndex(getFloatArrayFromSlice(output.slice(i)))));
            }
            //eval.eval(lables, predicted);

        //}

        return results;
    }

    private float[] getFloatArrayFromSlice(INDArray rowSlice) {
        float[] result = new float[rowSlice.columns()];
        for (int i = 0; i < rowSlice.columns(); i++) {
            result[i] = rowSlice.getFloat(i);
        }
        return result;
    }

    private static int maxIndex(float[] vals) {
        int maxIndex = 0;
        for (int i = 1; i < vals.length; i++) {
            float newnumber = vals[i];
            if ((newnumber > vals[maxIndex])) {
                maxIndex = i;
            }
        }
        return maxIndex;
    }


    public void loadDataset() throws Exception{
        createDatasetFromCsv();
        RecordReader rr = new CSVRecordReader(';');
        rr.initialize(new FileSplit(new File(pathDataSet)));

        trainIter = new RecordReaderDataSetIterator(rr,batchSize,14,4);
        model.fit( trainIter, nEpochs );
    }

    public Double parseAttribute(String attribute){
        Double value = null;
        try {
            value = Double.parseDouble(attribute);
        } catch (NumberFormatException e) {
            for (String key : transformMap.keySet()) {
                if (attribute.contains(key.toLowerCase())) {
                    value = transformMap.get(key).doubleValue();
                }
            }

            for (String key : fontFamilyMap.keySet()) {
                if (attribute.contains(key.toLowerCase())) {
                    value = fontFamilyMap.get(key).doubleValue();
                }
            }

            for (String key : fontStyleMap.keySet()) {
                if (attribute.contains(key.toLowerCase())) {
                    value = fontStyleMap.get(key).doubleValue();
                }
            }

            for (String key : fontWeightMap.keySet()) {
                if (attribute.contains(key.toLowerCase())) {
                    value = fontWeightMap.get(key).doubleValue();
                }
            }

            for(int i=0; i < classesMap.values().size();i++){
                if (attribute.contains(classesMap.get(i).toLowerCase())) {
                    value = new Double(i);
                }
            }
        }
        return value;
    }

    public void createDatasetFromCsv() throws IOException{

        FileReader fileReader = new FileReader(MyProperties.getInstance().getProperty("path.dataset.csv"));
        BufferedReader inStream = new BufferedReader(fileReader);
        String inString;

        BufferedWriter bw = new BufferedWriter(new FileWriter(pathDataSet, false));


        while ((inString = inStream.readLine()) != null) {

            String newLine = "";
            String[] lineParsed = inString.split(";");

            for(int i=0; i < lineParsed.length - 1; i++){
                String attribute = lineParsed[i].toLowerCase();
                attribute = attribute.replace("px","");

                Double value = parseAttribute(attribute);
                if(value == null){
                    value = 0.0;
                }

                newLine = newLine.concat(value.toString()).concat(";");

            }

            String clazz = lineParsed[lineParsed.length - 1];
            Double valueClass = 0.0;

            for(int i=0; i < classesMap.values().size();i++){
                if (clazz.contains(classesMap.get(i).toLowerCase())) {
                    valueClass = new Double(i);
                }
            }

            newLine = newLine.concat(valueClass.toString());

            bw.append(newLine + '\n');
        }
        bw.flush();
        inStream.close();
    }

    public static Deeplearning4j getInstance() throws  Exception{
       if (single_instance == null) {

            for(int i=0; i < valuesTransform.length; i++){
                transformMap.put(valuesTransform[i],i);
            }

            for(int i=0; i < valuesFontFamily.length; i++){
                fontFamilyMap.put(valuesFontFamily[i],i);
            }

            for(int i=0; i < valuesFontStyle.length; i++){
                fontStyleMap.put(valuesFontStyle[i],i);
            }

           for(int i=0; i < valuesFontWeight.length; i++){
               fontWeightMap.put(valuesFontWeight[i],i);
           }


           for(int i=0; i < classes.length; i++){
                classesMap.put(i,classes[i]);
            }

            single_instance = new Deeplearning4j();

            conf = new NeuralNetConfiguration.Builder()
                    .seed(123)
                    .weightInit(WeightInit.XAVIER.XAVIER)
                    .updater(new Adam(learningRate))
                    .list()
                    .layer(new DenseLayer.Builder().nIn(numInputs).nOut(numHiddenNodes)
                            .activation(Activation.RELU)
                            .build())
                    .layer(new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                            .activation(Activation.SOFTMAX)
                            .nIn(numHiddenNodes).nOut(numOutputs).build())
                    .build();
            model = new MultiLayerNetwork(conf);
            model.init();
            model.setListeners(new ScoreIterationListener(10));
            single_instance.loadDataset();
        }
        return single_instance;
    }
}
