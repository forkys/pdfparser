package pdf.parser.ml.classifiers;

import com.google.gson.JsonObject;
import org.jsoup.select.Elements;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface Classifiers {
    public JsonObject getPredictions(Elements elements) throws Exception;
    public void loadDataset() throws Exception;
    public void createDatasetFromCsv() throws FileNotFoundException, IOException;
}
