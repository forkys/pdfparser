package pdf.parser.main;

import pdf.parser.ml.classifiers.Deeplearning4j;
import pdf.parser.ml.classifiers.Stanford;
import pdf.parser.ml.classifiers.Weka;

public class TestingClassifiers {

    public static void main(String[] args) throws Exception {
        /** load data training**/
        //Stanford.getInstance().executeTesting();
        //Weka.getInstance().executeTesting();
        Deeplearning4j.getInstance().executeTesting();
    }
}
