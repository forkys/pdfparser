package pdf.parser.services.exceptions;

import org.springframework.http.HttpStatus;

public class UploadException extends Exception {
    private final HttpStatus restStatus;

    public UploadException(String message, HttpStatus restStatus) {
        super(message);
        this.restStatus = restStatus;
    }

    public HttpStatus getRestStatus()
    {
        return restStatus;
    }
}
