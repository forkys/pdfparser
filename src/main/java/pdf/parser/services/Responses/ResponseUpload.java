package pdf.parser.services.Responses;

public class ResponseUpload {
    private int statusCode;
    private String html;

    public ResponseUpload(int id, String html) {
        this.statusCode = id;
        this.html = html;
    }
}
