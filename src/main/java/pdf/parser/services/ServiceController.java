package pdf.parser.services;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.commons.io.FileUtils;
import org.reflections.Reflections;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pdf.parser.convert.PdfConvert;
import pdf.parser.elaborate.ElaborateDoc;
import pdf.parser.ml.classifiers.Classifiers;
import pdf.parser.ml.classifiers.Deeplearning4j;
import pdf.parser.ml.classifiers.Stanford;
import pdf.parser.ml.classifiers.Weka;
import pdf.parser.properties.MyProperties;
import pdf.parser.utils.UtilsFunctions;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ServiceController {


    private HttpServletRequest request;
    public ServiceController(){};

    @PostMapping("/save-rows")
    public ResponseEntity< String > saveRow(@RequestBody Map<String, Object> inputData) throws  IOException {

        try {
            ArrayList< LinkedHashMap<String,String>> listRows = (ArrayList< LinkedHashMap<String,String>>)inputData.get("rows");
            UtilsFunctions.getInstance().addNewRowsDataSet(listRows, (String) inputData.get("label"));
            return ResponseEntity.status(HttpStatus.OK).body("added");
        }catch(Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }


    @PostMapping("/create-data-set")
    @JsonIgnoreProperties
    public ResponseEntity< String > createDataSet(@RequestBody Map<String, Object> inputData) throws  IOException {

        try {
            byte[] decodedBytes;
            FileOutputStream fop;
            decodedBytes = new BASE64Decoder().decodeBuffer(inputData.get("file").toString());
            String pathFile = MyProperties.getInstance().getProperty("path.tmp.pdf") + inputData.get("name").toString();
            File file = new File(pathFile);
            fop = new FileOutputStream(file);

            fop.write(decodedBytes);

            fop.flush();
            fop.close();
            String textInHTML = new PdfConvert().getFileParsed(file);

            ElaborateDoc elaborateDoc = new ElaborateDoc(textInHTML);

            //Weka.getInstance().addRowsTraining(elaborateDoc.getElementsDoc());
            JsonObject results = new JsonObject();
            results.addProperty("parts",UtilsFunctions.getInstance().extractParts(elaborateDoc.getElementsDoc()).toString());
            return ResponseEntity.status(HttpStatus.OK).body(results.toString());
        }catch(Exception e){
            JsonObject errorM = new JsonObject();
            errorM.addProperty("message",e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorM.toString());
        }
    }

    @PostMapping("/elaborate-doc")
    public ResponseEntity< String > handleFileUpload(@RequestBody Map<String, Object> inputData) throws  IOException {

        try {
            byte[] decodedBytes;
            FileOutputStream fop;
            decodedBytes = new BASE64Decoder().decodeBuffer(inputData.get("file").toString());
            String pathFile = MyProperties.getInstance().getProperty("path.tmp.pdf") + inputData.get("name").toString();
            File file = new File(pathFile);
            fop = new FileOutputStream(file);

            fop.write(decodedBytes);

            fop.flush();
            fop.close();
            String textInHTML = new PdfConvert().getFileParsed(file);

            ElaborateDoc elaborateDoc = new ElaborateDoc(textInHTML);
            JsonObject resultsML;

            switch(MyProperties.getInstance().getProperty("classifier")){
                case "weka":{ resultsML = Weka.getInstance().getPredictions(elaborateDoc.getElementsDoc());
                    break;
                }
                case "stanford":{
                    resultsML = Stanford.getInstance().getPredictions(elaborateDoc.getElementsDoc());
                    break;
                }
                default:{
                    resultsML = Deeplearning4j.getInstance().getPredictions(elaborateDoc.getElementsDoc());
                    break;
                }
            }

            return ResponseEntity.status(HttpStatus.OK).body(resultsML.toString());
        }catch(Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/list-classifiers")
    public ResponseEntity< String[] > getClassifiers(){
        String setted = MyProperties.getInstance().getProperty("classifier");
        Set<String> classes = UtilsFunctions.getInstance().getAllClassifiers(Classifiers.class);
        String[] response = new String[classes.size()];
        int i=1;
        Iterator iterator = classes.iterator();
        while(iterator.hasNext()){
            String clazz = iterator.next().toString();
            if(clazz.toLowerCase().contains(setted)){
                response[0] = clazz;
            }else{
                response[i] = clazz;
                i++;
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    };

    @GetMapping("/get-parts-pdf")
    public ResponseEntity< String > getPartsPdf(){
        return ResponseEntity.status(HttpStatus.OK).body(MyProperties.getInstance().getProperty("pdf.elements"));
    };

    @PostMapping("/change-classifier")
    public ResponseEntity< String > setClassifier(@RequestBody Map<String, Object> inputData){
        try {
            MyProperties.getInstance().setProperty("classifier", inputData.get("newClassifier").toString().toLowerCase());
            return ResponseEntity.status(HttpStatus.OK).body("changed");
        }catch (IOException e ){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    };

}
