package pdf.parser.properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class MyProperties {

    String fileProperties = "C:\\Users\\davide.forletta\\Desktop\\tesi\\pdfparser\\data\\properties\\myproperties.properties";
    public Properties properties = new Properties();
    private static MyProperties ourInstance = new MyProperties();

    public static MyProperties getInstance(){

        if (ourInstance == null)
            ourInstance = new MyProperties();
        return ourInstance;
    }

    private MyProperties()  {
        try {
            properties.load(new FileInputStream(fileProperties));
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public String getProperty(String property){
        return properties.getProperty(property);
    }

    public void setProperty(String property, String newValue) throws IOException{
        properties.setProperty(property,newValue);
    }
}
