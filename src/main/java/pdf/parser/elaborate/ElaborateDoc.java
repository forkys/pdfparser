package pdf.parser.elaborate;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.Map;

public class ElaborateDoc {

    public ElaborateDoc(String textDoc)
    {

        this.highilighter.put("title","#FFFF00");
        this.textDoc = Jsoup.parse(textDoc);
    };

    public void addAttributesDoc(Map<Integer,String> results){
        Elements elements = getElementsDoc();
        /*for(String tag: tags){
            elements.addAll(textDoc.body().getElementsByTag(tag));
        }*/
        for(int i=0; i < elements.size(); i++){
            Element singleElement = elements.get(i);
            singleElement.attr("style", "background-color:" + this.highilighter.get(results.get(i)));
        }
    }

    public Element getBodyDoc(){
        return this.textDoc.body();
    }

    public Elements getElementsDoc(){
        Elements elements = new Elements();
        for(String tag: tags){
            for(Element newElement: textDoc.body().getElementsByTag(tag)){
                    if(newElement.childNodes().size()==0 || (newElement.childNodes().size()==1 && (newElement.childNodes().get(0) instanceof TextNode))){
                    elements.add(newElement);
                }
            }
           // elements.addAll(textDoc.body().getElementsByTag(tag));
        }
        return elements;
    }

    public Document textDoc;

    String[] tags = new String[]{"span","p"};

    Map<String,String> highilighter = new HashMap<>();
}
