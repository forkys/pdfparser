package pdf.parser.convert;

import org.apache.pdfbox.pdmodel.PDDocument;
import pdf.parser.convert.libs.IhtmlConvert;
import pdf.parser.convert.libs.Images2HTML;
import pdf.parser.convert.libs.PDFText2HTML;
import pdf.parser.convert.libs.StatisticParser;

import java.io.*;

public class PdfConvert implements IhtmlConvert {

    public void convert(File srcFile, String desPath) {
        final String realName = srcFile.getName();
        final String htmlPath = desPath;
        PDDocument document = null;
        Writer output = null;
        try {
            document = PDDocument.load(srcFile);
            document.setAllSecurityToBeRemoved(true);
            output = new PrintWriter(new FileOutputStream(desPath));
            //output = new OutputStreamWriter(new FileOutputStream(htmlPath + realName + ".html"),"UTF-8");
            StatisticParser statisticParser = new StatisticParser();
            statisticParser.writeText(document, new Writer() {
                @Override
                public void write(char[] cbuf, int off, int len) throws IOException {
                }
                @Override
                public void flush() throws IOException {
                }
                @Override
                public void close() throws IOException {
                }
            });
            Images2HTML image = null;
            image = new Images2HTML();
            File imgDir = new File(htmlPath + realName + File.separator);
            if(!imgDir.exists() && !imgDir.isDirectory()) imgDir.mkdirs();
            image.setBasePath(imgDir);
            image.setRelativePath(realName);
            image.processDocument(document);

            PDFText2HTML stripper = new PDFText2HTML("UTF-8", statisticParser);
            stripper.setForceParsing(true);
            stripper.setSortByPosition(true);
            //stripper.setImageStripper(image);
            stripper.writeText(document, output);
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (document != null) {
                try {
                    document.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public String getFileParsed(File srcFile) {
        final String realName = srcFile.getName();
        PDDocument document = null;
        Writer output = null;
        try {
            document = PDDocument.load(srcFile);
            document.setAllSecurityToBeRemoved(true);
            StatisticParser statisticParser = new StatisticParser();
            statisticParser.writeText(document, new Writer() {
                @Override
                public void write(char[] cbuf, int off, int len) throws IOException {
                }
                @Override
                public void flush() throws IOException {
                }
                @Override
                public void close() throws IOException {
                }
            });
            Images2HTML image = null;
            image = new Images2HTML();
            File imgDir = new File(srcFile.getAbsolutePath() + realName + File.separator);
            if(!imgDir.exists() && !imgDir.isDirectory()) imgDir.mkdirs();
            image.setBasePath(imgDir);
            image.setRelativePath(realName);
            image.processDocument(document);

            PDFText2HTML stripper = new PDFText2HTML("UTF-8", statisticParser);
            stripper.setForceParsing(true);
            stripper.setSortByPosition(true);
            stripper.setImageStripper(image);
            return stripper.getText(document);
            //stripper.writeText(document, output);
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (document != null) {
                try {
                    document.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}