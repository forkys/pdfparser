package pdf.parser.convert.libs;

import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

public class Image {
    public PDXObjectImage image;
    public Float x;
    public String name;
}
