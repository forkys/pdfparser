package pdf.parser.convert.libs;

import java.io.File;

/**
 * @Description
 * @date 2013年12月16日
 * @author WangXin
 */
public interface IhtmlConvert {
    public void convert(File srcFile, String desPath);
}