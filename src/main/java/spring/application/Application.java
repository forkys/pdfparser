package spring.application;

import org.apache.commons.io.FileUtils;
import org.reflections.Reflections;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import pdf.parser.ml.classifiers.Classifiers;
import pdf.parser.ml.classifiers.Deeplearning4j;
import pdf.parser.ml.classifiers.Stanford;
import pdf.parser.ml.classifiers.Weka;
import pdf.parser.properties.MyProperties;
import pdf.parser.services.ServiceController;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@SpringBootApplication
@ComponentScan(basePackageClasses = ServiceController.class)
public class Application {

    public static void main(String[] args) throws Exception {
        /** load data training**/
        SpringApplication.run(Application.class, args);
        FileUtils.cleanDirectory(new File(MyProperties.getInstance().getProperty("path.stanford.tmp.predicted")));
        FileUtils.cleanDirectory(new File(MyProperties.getInstance().getProperty("path.weka.tmp.predicted")));
        FileUtils.cleanDirectory(new File(MyProperties.getInstance().getProperty("path.deeplearning.tmp.predicted")));
        FileUtils.cleanDirectory(new File(MyProperties.getInstance().getProperty("path.tmp.pdf")));
        Stanford.getInstance().loadDataset();
        Weka.getInstance().loadDataset();
        Deeplearning4j.getInstance().loadDataset();
    }
}
